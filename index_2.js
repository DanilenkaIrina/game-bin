/*2. Напишите клиентское веб-приложение, состоящее из корзины и множества элементов корзины в окне браузера. 
Необходимо реализовать следующие функциональные возможности:
* Перетаскивание элементов корзины
*Открытие ведра, когда мусорный элемент перетаскивается по нему и закрытие ведра, когда мусорный элемент перетаскивается мимо ведра (т.е. был над ведром, но покинул его и перетаскивается дальше), или опускается в ведро
* Выбросить мусорный элемент в ведро, означает заставить его исчезнуть из окна браузе-ра.
*/


const pieces = document.getElementById('pieces');
const bin = document.getElementById('bin');
let count = 1;
let offsetX, offsetY;

for(let i = 0; i <= 10; i++) {
    const img = document.createElement('img');
    img.id = 'drag' + count;
    img.src = 'piece.png';
    img.setAttribute('draggable', 'true');
    img.setAttribute('ondragstart', 'start(event)');
    img.setAttribute('ondragend', 'end(event)');

    img.style.top = Math.floor(Math.random() * (600 - 1 + 1)) + 1 + 'px';
    img.style.left = Math.floor(Math.random() * (1000 - 250 + 1)) + 250 + 'px';
    count++;
    pieces.append(img);
}

const start = (event) => {
    offsetX = event.offsetX;
    offsetY = event.offsetY;
}

const end = (event) => {
    event.target.style.top = (event.pageY - offsetY) + 'px';
    event.target.style.left = (event.pageX - offsetX) + 'px';

    if((Number(event.target.style.top.slice(0,-2)) >= 150 && Number(event.target.style.top.slice(0,-2)) <= 230) 
    && (Number(event.target.style.left.slice(0,-2)) >= 145 && Number((event.target.style.left.slice(0,-2)) <= 200))) {
        event.target.remove();
    } else {
        event.target.style.top = event.pageY + 'px';
        event.target.style.left = event.pageX + 'px';
        event.target.style.zIndex = "1";
    }
    event.target.style.top = (event.pageY - offsetY) + 'px';
    event.target.style.left = (event.pageX - offsetX) + 'px';
}

const binOpen = () => {
    bin.src = 'open.jpg';
}

const binClose = () => {
    bin.src = 'close.jpg';
}